#!/usr/bin/env python3
#
# ./plot.py out-aniso-maxwellians-0-Q2-adaptive_tol2-P1_AMR2
#
import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as npy
import matplotlib.ticker as ticker
import random as rand
from numpy import array
import locale
import pandas as pd
#import seaborn as sns

locale.setlocale(locale.LC_ALL, '')
#plt.rcParams["font.size"] = 11
#plt.rcParams["font.weight"] = "bold"
#plt.rcParams["axes.labelweight"] = "bold"
#plt.rcParams.update({'font.size': 11})
#plt.rcParams.update({'font.weight': "bold"})
ncells_str = "0"
nips_str = "0"
P_or_Q = "?"
neq = 0
nfiles = 0
t_0_arr = ["" for x in range(10)]
err_arr = npy.zeros(10)
Times = npy.zeros([10,8]) # num cells, P/Q, Jac, solve, IPs, Time tot, neq/#batch, nnz ave
max_steps=2000000
for filename in sys.argv[1:]:
    Temps = npy.zeros([max_steps,5])
    NRLTemps = npy.zeros(max_steps)
    NRLTimes = npy.zeros(max_steps)
    NRLTemps2 = npy.zeros([max_steps,4])
    NRLTimes2 = npy.zeros(max_steps)
    parts = filename.split('-')
    print('file = ',parts)
    iso_type = parts[1]
    shift_type = parts[2]
    shift_type_2 = shift_type.split('_')
    #print ('shift_type_2 = ',shift_type_2)
    if len(shift_type_2) > 1:
        shift_type = shift_type_2[0]
        shift_name = ', ' + shift_type_2[0] + ' ' + shift_type_2[1]
    else: shift_name = ''
    normal_species_type = parts[3]
    elem_type = parts[4]
    Times[nfiles,1] = int (elem_type[1])
    if P_or_Q == '?' : P_or_Q = elem_type[0]
    elif P_or_Q != elem_type[0] : print('ERROR: can not mix P and Q', P_or_Q, elem_type[0])
    ts_type = parts[5]
    ts_type_rtol = ts_type.split('_')
    ts_type_name = ts_type_rtol[0] + '-' + ts_type_rtol[1]
    if len(parts) > 7 :
        mparts = parts[7].split('_')
        #print('   2) parts = ',parts,', mparts=',mparts)
        machine_full = parts[7].replace("_"," ")
        if len(mparts) > 1: mach_short = '-' + mparts[1]
        else: mach_short = '-' + mparts[0]
    else :
        machine_full = 'unkown machine'
        mach_short = '-unkown'
    aux_long = ''
    adx = ''
    if len(parts) > 8 :
        aux = '-' + parts[8]
        aux_long = ', ' + parts[8] + ' grid'
        #print ('aux= ',aux)
    #print (ts_type_rtol[1])
    #print (ts_type_rtol[1][0:3])
    if (len(ts_type_rtol) > 1):
        tol = int(ts_type_rtol[1][3])
        ts_type_name_full = ts_type_rtol[0] + r' TS (rtol=$10^{{{:d}}}$)'.format(-tol)
    else: ts_type_name_full = ts_type_rtol[0] + ' TS'
    #print(ts_type_name_full)
    amrparts = parts[6].split('_')
    amr_type = amrparts[1][3]
    #print ('AMR = ',amr_type)
    #print(r'$10^{{{:d}}}$'.format(-tol))
    # if ts_type_rtol[0] == 'adaptive' and ts_type_rtol[1][0:4] == 'rtol': ts_type = '.' +  ts_type[1:]
    if normal_species_type == '0':
        normal_species = 'electron normalized'
        normal_species_short = 'normalize-e'
        xlabel = r'Time ($\tau_e$), '
    else:
        normal_species = 'ion normalized'
        normal_species_short = 'normalize-i'
        xlabel = r'Time ($\tau_i$), '
    err_arr[nfiles] = 1000
    idx = 0
    idx_i = 0
    idx_e = 0
    idx2 = 0
    for text in open(filename,"r"): 
        words = text.split()
        n = len(words)
        #if n > 1 and words[0] == 'Option': print (words)
        if n > 2 and words[1] == 'FormLandau':
            ncells_str = words[4]
            nips_str = words[2]
            neq = int (words[15].split('=')[1]) / n_batched
        elif n > 1 and words[0] == 'step':
            #print('GOT STEP: n=',n,'words: ',words)
            # 0      1    2      3           4          5       6      7      8        9        10      11         12     13    14 15         16     17         18      19          20         21         22          23          24         25         26          27         28         29
            #step    0) time= 0.000000e+00 temperature (eV): electron: T= 3.5817e+02 T_par= 3.0001e+02 T_perp= 3.8725e+02 ;ion: T= 2.3878e+02 T_par= 2.0001e+02 T_perp= 2.5816e+02 NRL_i_par= 2.0000e+02 NRL_i_perp= 2.6000e+02 NRL_e_par= 3.0000e+02 NRL_e_perp= 3.9000e+02
            #step    0) time= 0.000000e+00 temperature (eV): electron: T= 3.5817e+02 T_par= 3.0001e+02 T_perp= 3.8725e+02 ;ion: T= 2.3878e+02 T_par= 2.0001e+02 T_perp= 2.5816e+02 NRL_i_par= 2.0000e+02 NRL_i_perp= 2.6000e+02
            #step    0) time= 0.000000e+00 temperature (eV): electron: T= 3.5817e+02 T_par= 3.0001e+02 T_perp= 3.8725e+02 ;ion: T= 2.3878e+02 T_par= 2.0001e+02 T_perp= 2.5816e+02 NRL_i_par= 2.0000e+02 NRL_i_perp= 2.6000e+02

            Temps[idx,0] = float(words[3])
            #if idx==1: dt0 = Temps[idx-1,0] = Temps[idx,0] / 10
            Temps[idx,1] = float(words[10])
            Temps[idx,2] = float(words[12])
            Temps[idx,3] = float(words[17])
            Temps[idx,4] = float(words[19])
            if idx > 20:
                err = 0
                for ii in range(4):
                    if abs(Temps[idx,ii+1]-300) > err: err = abs(Temps[idx,ii+1]-300)
                # if err < err_arr[nfiles]:
                err_arr[nfiles] = err
            idx = idx+1
            if n > 21:
                NRLTemps[idx_i] = float(words[21]) # par - ions
                NRLTimes[idx_i] = float(words[3]) # par
                idx_i = idx_i+1
                NRLTemps[idx_i] = float(words[23]) # perp
                NRLTimes[idx_i] = float(words[3]) # par
                idx_i = idx_i+1
                if n > 25:
                    idx_e = idx_e+2
                    NRLTemps[idx_e] = float(words[25]) # par - e
                    NRLTimes[idx_e] = float(words[3]) # par
                    idx_e = idx_e+1
                    NRLTemps[idx_e] = float(words[27]) # perp
                    NRLTimes[idx_e] = float(words[3]) # par
                    idx_e = idx_e+1
                    idx_i = idx_i+2
            if idx >= max_steps: break
        elif n > 1 and words[0] == 'thermal':
            t_0_arr[nfiles] = words[18]
            n_batched = int (words[19]) # before N so stash now
        elif n > 1 and words[0] == 'KSPSolve':
            Times[nfiles,3] = float(words[3])
        elif n == 5 and words[0] == 'Time':
            Times[nfiles,5] = float(words[2])
        elif n > 1 and words[0] == 'Landau' and words[1] == 'Operator':
            Times[nfiles,2] = float(words[4])
        elif n > 3 and words[0] == 'nrl-step':
            # 0        1      2    3          4    ...
            # nrl-step 99993 time= 0.00313607 310.768 310.768 289.232 289.232
            NRLTimes2[idx2] = float(words[3])
            for ii in range(4):
                NRLTemps2[idx2,ii] = float(words[4+ii])
            idx2 = idx2 + 1
            if idx2 >= max_steps: break
    #print ('Temps = ',Temps[:idx,1:])
    #print ('times = ',Temps[:idx,0])
    #print ('NRL 2 temps = ',NRLTemps2[:idx2,0])
    #print ('NRL 2 times = ',NRLTimes2[:idx2])
    series_name = [r'$T_{e,par}$', '$T_{e,perp}$', '$T_{i,par}$', '$T_{i,perp}$']
    series_name2 = ['NRL']
    series_name3 = ['NRL (p)']
    t_0 = float(t_0_arr[nfiles])
    #print (t_0 * Temps[:idx,0])
    #marks = ['s','o', 'D', 'P']
    styles = ['bD-','bD:', 'go-', 'go:']
    styles2 = ['k:']
    styles3 = ['r:']
    #
    # plot
    #
    plt.rcParams["figure.dpi"] = 300
    #plt.rcParams["figure.figsize"] = [5.00, 4.0]
    #plt.rcParams["figure.autolayout"] = True
    ylabel = 'Temperature (eV)'
    df = pd.DataFrame(data=Temps[:idx,1:], index=(Temps[:idx,0] * t_0), columns=series_name)
    if idx2 > 0:
        df2 = pd.DataFrame(data=NRLTemps2[:idx2,0], index=NRLTimes2[:idx2] * t_0, columns=series_name3)
        df3 = pd.DataFrame(data=NRLTemps2[:idx2,1], index=NRLTimes2[:idx2] * t_0, columns=series_name3)
        df4 = pd.DataFrame(data=NRLTemps2[:idx2,2], index=NRLTimes2[:idx2] * t_0, columns=series_name3)
        df5 = pd.DataFrame(data=NRLTemps2[:idx2,3], index=NRLTimes2[:idx2] * t_0, columns=series_name3)
        st = styles3
    elif idx_i > 0:
        df2 = pd.DataFrame(data=NRLTemps[:idx_i:4],  index=NRLTimes[:idx_i:4] * t_0, columns=series_name2)
        df3 = pd.DataFrame(data=NRLTemps[1:idx_i:4], index=NRLTimes[:idx_i:4] * t_0, columns=series_name2)
        df4 = pd.DataFrame(data=NRLTemps[2:idx_e:4], index=NRLTimes[:idx_e:4] * t_0, columns=series_name2)
        df5 = pd.DataFrame(data=NRLTemps[3:idx_e:4], index=NRLTimes[:idx_e:4] * t_0, columns=series_name2)
        st = styles2
    ax = df.plot(lw=1, colormap='jet', style=styles, markersize=1, logx=True, logy=False, grid=True, legend=False)
    #print ('idx=',idx_e,', idx2=',idx2)
    if idx_i > 0 or idx2 > 0:
        df2.plot(ax=ax,lw=1, colormap='jet', style=st, markersize=1, logx=True, logy=False, grid=True, legend=False)
        df3.plot(ax=ax,lw=1, colormap='jet', style=st, markersize=1, logx=True, logy=False, grid=True, legend=False)
        df4.plot(ax=ax,lw=1, colormap='jet', style=st, markersize=1, logx=True, logy=False, grid=True, legend=False)
        df5.plot(ax=ax,lw=1, colormap='jet', style=st, markersize=1, logx=True, logy=False, grid=True, legend=False)
    title='Anisotropic' + shift_name + ', ' + ncells_str + ' ' + elem_type + ' cells, ' + machine_full + aux_long
    ax.set_title(title,pad=20, fontdict={'fontsize':14}) # , fontdict={'fontsize':16}
    patches, labels = ax.get_legend_handles_labels()
    xmin, xmax, ymin, ymax = plt.axis()
    xmin, xmax, ymin, ymax = plt.axis([xmin, xmax, 175, 400])
    ax.legend(patches, labels, loc='best') # , fontsize=28) #, fontsize=14
    plt.legend(npy.unique(labels),title='Temperature', title_fontsize=10, fontsize=10)
    plt.tight_layout()
    #ax.set_xlabel(xlabel + normal_species + r', ($t_0=$' + t_0_arr[nfiles] + ')') #, fontdict={'fontsize':16})
    ax.set_xlabel('Time (seconds) ' +  r', ($t_0=$' + t_0_arr[nfiles] + ' sec)', fontdict={'fontsize':16}) #, fontdict={'fontsize':16})
    ax.set_ylabel(ylabel, fontdict={'fontsize':16}) #, fontdict={'fontsize':16})
    plt.savefig('temperature-relaxation-' + normal_species_short + '-'+ elem_type + '-'+ ts_type_name + '-'+ shift_type + mach_short + '-AMR' + amr_type + aux + '.png', bbox_inches='tight')
    #print('ncells = ',ncells_str,', nips = ',nips_str,', N = ', int(neq))
    plt.show()
    Times[nfiles,6] = int(neq)
    Times[nfiles,0] = int(ncells_str)
    Times[nfiles,4] = int(nips_str)
    nfiles = nfiles + 1
    if nfiles >= 10: break
### end files
#latex table
errors = err_arr[:nfiles]/300*100
#print('errors = ',errors,'\n')
#if P_or_Q == 'Q' : 
#    Times[:nfiles,7] = [14.3, 22.7, 31.4]
# else :
#    Times[:nfiles,7] = [10.8, 15.5, 20.8]
df = pd.DataFrame({
    "# cells": Times[:nfiles,0],
    "# IPs": Times[:nfiles,4],
    "# eqs": Times[:nfiles,6],
    "nnz/row": 0, # Times[:nfiles,7],
     P_or_Q: Times[:nfiles,1],
    "Jacobian": Times[:nfiles,2],
    "Solve": Times[:nfiles,3],
    "Total time": Times[:nfiles,5],
    "error (%)": errors})
#def f1(x):
#    return '%d' % x
#def f2(x):
#    return '%.2f' % x
#def f3(x):
#    return ",%.2f" % x
#df['\# cells'] = df['\# cells'].map("{:,.0f}".format)
#df['\# IPs'] = df['\# IPs'].map("{:,.0f}".format)
#df['Q'] = df['Q'].map("{:,.0f}".format)
#df['Jacobian'] = df['Jacobian'].map("{:,.0f}".format)
#df["Solve"] = df["Solve"].map("{:,.0f}".format)
#df['Total time'] = df['Total time'].map("{:,.0f}".format)
df['error (%)'] = df['error (%)'].map("{:,.2f}".format)
df['nnz/row'] = df['nnz/row'].map("{:,.1f}".format)

#def make_pretty(styler):
#    styler.background_gradient(axis=None, vmin=1000, vmax=5000, cmap="YlGnBu")
#df.loc["Jacobian":"Total time"].style.pipe(make_pretty)

#cm = sns.light_palette("green_r", as_cmap=True)
#styler = df.style.hide(axis="index")
#styler = df.style.background_gradient(axis=0, low=0.75, high=1.0, cmap='YlOrRd').hide(axis="index")
#df.style.background_gradient(axis=0, gmap=df['Solve'], cmap='YlOrRd')
#styler = df.style.hide(axis="index").background_gradient(subset=(df['Solve']>11, "Solve"))
styler = df.style.hide(axis="index").background_gradient(cmap='RdYlGn_r', subset=['Jacobian', 'Solve', 'Total time'], low=.1, high=.1).background_gradient(cmap='RdYlGn_r', subset=['error (%)'], low=.1, high=.1).format(precision=0, thousands=",")

print(styler.to_latex(convert_css=True,column_format='cccccrrrr', position='h!', hrules=True, caption = 'Anisotropic thermalization timings (seconds) on ' + machine_full, label='tab:timings'+mach_short+P_or_Q))

