#!/usr/bin/env python3
#
# plot out11_* where out11_2D_001_Aggregate|Batch-Kokkos|PETSc_TFQMR.txt
#
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
import pandas as pd
import seaborn as sns

krylov2 = 'dummy'
fields = np.zeros( (2,12) ) # dim idx ; nits
nfiles = 0
max_bid = 0
max_solve_idx = 0
batch_size = np.zeros( (2) )
for filename in sys.argv[1:]:
    print (filename)
    base = filename.split('.')
    words = base[0].split('_')
    nfiles += 1
    row_id = int(words[1][0]) - 2 # dim
    solver = words[3]
    krylov = words[4]
    bsz = float(words[2]) # n batch
    if solver == 'Batch-Kokkos': solve_idx = 0
    else : solve_idx = 1
    batch_size[row_id] = bsz
    batch_nits = 0
    nl_3_count = 0
    for text in open(filename,"r"):
        words = text.split()
        n = len(words)
        if len(words[0].split(':')) == 2 and solve_idx == 0 : # batch
            try: 
                s_id = int(words[0].split(':')[0])
            except ValueError:
                print ('NOT int: ',words)
            else:
                arr_its = words[1:]
                desired_array = [int(numeric_string) for numeric_string in arr_its]
                fields[row_id,s_id + 2] = sum(desired_array) / bsz
        elif n > 5 and words[3] == 'nonlinear' : # solver iterations=' :
            nl_3_count = nl_3_count + 1
            if nl_3_count == 1 : 
                nl_nits = int(words[5].split('=')[1])
        elif n > 5 and words[3] == 'linear' and nl_3_count == 1 : # solver iterations=' :
            nits = int(words[5].split('=')[1])
            if solve_idx == 0 : # batch
                batch_nits = nits
            else : # aggragate
                fields[row_id,0] = nits
    if solve_idx == 0 : # batch
        print (row_id, batch_nits)
        fields[row_id,1] = batch_nits / (10 * bsz * nl_nits)
    else :
        fields[row_id,0] = fields[row_id,0] / nl_nits # aggragate
if nfiles == 0: print('no files: Usage plot.py out*')
num_batch = ['Agg.', 'Bat.', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
dim_strs = ['2', '3']
solve_names = ['Batch ' + krylov, 'Ensemble ' + krylov]
type_tag = [krylov.lower(),'aggregate']

#    solver = solve_names[solve_idx]
#
dfc = pd.DataFrame(fields[:,:], index=dim_strs, columns=num_batch[:])
dfc.columns.name = 'V'
#dfp['Krylov its'] = dfp['Krylov its'].map('{:,.0f}'.format)
print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.1f}".format, caption='Average ' + krylov +  ' iterations per linear solve for aggregate (Agg.) and batch (Bat.) solvers in 2 and 3V, with batch solver iteration count averages for each species 1-10. 2V batch size = ' + str(int(batch_size[0])) + ' and 3V = ' + str(int(batch_size[1])) + '.', label='tab:ave-nits'))

