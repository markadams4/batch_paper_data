#!/usr/bin/env python3
#
# plot out11_* where out11_2D_008_Batch-Kokkos_Tfqmr_Perlmutter_1_NVIDIA-A100.txt: batch size 4, KK GMRES, on Perlmutter, 1 GPU, NVIDIA-A100 GPU
#
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
import pandas as pd

fields = np.zeros( (13,6) )
#             0    1    2    3    4     5     6     7      8     9    
hw_names = ['DRAM (GB/s)','L1 (TB/s)','L2 (GB/s)','dadd/cycle','dfma/cycle','dmul/cycle','TFlop/sec','AI-L1','Roofline-L1 \%','AI-L2','Roofline-L2 \%','AI-DRAM','RF-DRAM \%']
part_names = ['Jac-2V', 'M-2V', 'Sol-2V','Jac-3V', 'M-3V', 'Sol-3V']
#pd.options.display.float_format = '{:,}'.format

fields[0,:] = [75.8,  1230,  28.18, 38.33, 946, 538] 
fields[1,:] = [1.92,  3.58,  1.43,  1.92,  2.39,  1.59] 
fields[2,:] = [747, 4010,  881,   266,   2810,  1870] 
fields[3,:] = [163, 155,   156,   76.2,  91.8, 35.12] 
fields[4,:] = [1155,  0,     168,   546,   0,     36.11] 
fields[5,:] = [526, 329,   64.5,  305,   198, 3.14] 
fields[6,:] = [4.23,  0.68,  0.89,  2.06,  0.41,  0.16] 
fields[7,:] = [2.20,  0.19,  0.50,  1.07,  0.17,  0.10] 
fields[8,:] = [43.6,  18.27, 9.18,  21.27, 12.19, 8.11] 
fields[9,:] = [5.66,  0.17,  0.72,  7.75,  0.15,  0.08] 
fields[10,:] = [43.6, 54.2,  16.70, 21.27, 38.0,  25.3] 
fields[11,:] = [55.8, 0.56,  23.60, 53.8,  0.43,  0.29] 
fields[12,:] = [43.6, 63.6,  9.18,  21.3,  48.9,  27.8] 

dfp = pd.DataFrame(fields, index=hw_names, columns=part_names)
#pd.applymap(lambda x: str(int(x)) if abs(x - int(x)) < 1e-6 else str(round(x,2)))
#dfp.apply(lambda x: x.astype(int) if np.allclose(x, x.astype(int)) else x)
pd.options.display.float_format = lambda x : '{:.0f}'.format(x) if round(x,0) == x else '{:,.2f}'.format(x)
dfp.columns.name = 'Data'
#dfp['Krylov its'] = dfp['Krylov its'].map('{:,.0f}'.format)
print (dfp.to_latex(longtable=False, escape=False, caption='Roofline data: Jacobian (Jac), Mass (M), Solver (Sol)', label='tab:rooflinedata'))

#  float_format="{:,.2f}".format
